import quip from "quip";
import App from "./App.jsx";
import Styles from "./App.less";

quip.apps.initialize({
    initializationCallback: function(rootNode) {
        //ReactDOM.render(<App/>, rootNode);
        rootNode.innerHTML = 
        '<iframe src="https://spotfire-next.cloud.tibco.com/spotfire/wp/OpenAnalysis?file=/Samples/Analyzing%20Stock%20Performance&configurationBlock=SetPage%28pageIndex%3D0%29%3B&options=7-1,9-1,10-1,11-1,12-1,13-0,14-1,1-1,2-1,3-1,4-1,5-1,6-0,15-1,17-1" width="800" height="600" ></iframe>';
        var iframe = rootNode.firstChild;
        quip.apps.registerEmbeddedIframe(iframe);
    },
});
